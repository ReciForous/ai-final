import random
import inquirer
import crayons
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from circuit import (Circuit, ConnectionMatrix)
from logicGates import (AND, OR, NOT, XOR)
from connection import Connection
from solution import Solution
import traceback

# TODO: Write functions for multi point crossover and davis order crossover


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar.

    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


class GeneticAlgorithm(object):
    def __init__(
        self,
        max_gens=None,
        population=None,
        mutation_chance=1,
        selection=None,
        circuit_size=10
    ):
        self.max_gens = max_gens
        self.population = population
        self.mutation_chance = mutation_chance
        self.circuit_size = circuit_size
        self.parentGen = []
        self.finalGen = None
        self.currentGen = []
        self.optimumSolution = None
        self.selection = selection

    def generateParents(self, iput_list=None):
        for individual in range(self.population):
            solution = Solution()
            circuit_size = random.randint(len(iput_list), 30)
            solution.generateCircuit(
                no_of_connections=circuit_size,
                iput_list=iput_list
            )
            self.parentGen.append(solution)
            self.currentGen.append(solution)

    def rouletteWheel(self, previous_gen):
        total_pop_fitness = 0
        probabilities = 0

        # Find total fitness of a population
        for individual in previous_gen:
            total_pop_fitness += individual.fitness

        # Create roulette wheel
        relative_fitness = [individual.fitness / total_pop_fitness for individual in previous_gen]
        probabilities = [
            sum(relative_fitness[:i + 1]) for i in range(len(relative_fitness))
        ]

        parentA = None
        parentB = None

        # generate random selection number
        # Select parent A
        selected_fitness = random.random()
        for(i, individual) in enumerate(previous_gen):
            if selected_fitness <= probabilities[i]:
                parentA = individual
                break

        # Select parent B
        selected_fitness = random.random()
        for(i, individual) in enumerate(previous_gen):
            if selected_fitness <= probabilities[i]:
                parentB = individual
                break

        while parentA == parentB:
            selected_fitness = random.random()
            for(i, individual) in enumerate(previous_gen):
                if selected_fitness <= probabilities[i]:
                    parentB = individual
                    break
        # print("I've exited")
        # parentA = previous_gen[random.randint(0, len(previous_gen) - 1)]
        # parentB = previous_gen[random.randint(0, len(previous_gen) - 1)]

        # while parentA == parentB:
        #     parentB = previous_gen[random.randint(0, len(previous_gen) - 1)]

        return parentA, parentB

    def tournamentSelection(self, previous_gen):
        select_amount = random.randint(2, len(previous_gen) - 1)
        selected_individuals = []

        for i in range(select_amount):
            individual = previous_gen[random.randint(0, len(previous_gen) - 1)]

            while individual in selected_individuals:
                individual = previous_gen[random.randint(0, len(previous_gen) - 1)]

            selected_individuals.append(
                individual
            )
        selected_individuals.sort(key=lambda individual: individual.fitness)
        parentA = selected_individuals[0]
        parentB = selected_individuals[1]

        return parentA, parentB

    def rankedSelection(self):
        pass

    def start(self, training_iputs, training_outputs):
        # Generate randomized parent candidates
        gens = []
        total_fitness_per_gen = []
        self.generateParents(iput_list=training_iputs[0])
        self.calculateGenerationFitness(training_iputs, training_outputs)

        previous_gen = self.currentGen
        most_passes = 0
        median_connections_per_gen = []
        for gen in range(self.max_gens):
            self.currentGen = []
            while len(self.currentGen) < self.population:
                if self.selection == 'roulette':
                    parentA, parentB = self.rouletteWheel(previous_gen=previous_gen)
                elif self.selection == 'tournament':
                    parentA, parentB = self.tournamentSelection(previous_gen=previous_gen)
                childA, childB = self.crossover(parentA=parentA, parentB=parentB)
                self.currentGen.append(childA)
                self.currentGen.append(childB)

            for individual in self.currentGen:
                self.mutate(circuit=individual.circuit)

            total_fitness, highest_passed, median_connections = self.calculateGenerationFitness(training_iputs, training_outputs)
            if highest_passed > most_passes:
                most_passes = highest_passed
            previous_gen = self.currentGen

            total_fitness_per_gen.append(total_fitness)
            median_connections_per_gen.append(median_connections)
            gens.append(gen)
            printProgressBar(gen, self.max_gens - 1, prefix='Progress:', suffix='Complete', length=25)
        print()
        self.finalGen = self.currentGen

        return gen, total_fitness_per_gen, gens, highest_passed, median_connections_per_gen

    def test(self, test_set_iputs, test_set_outputs):
        pass

    def calculateGenerationFitness(self, training_iputs, training_outputs):
        fitness = 0
        highest_passed = 0
        median_connections = 0
        for individual in self.currentGen:
            individual.calculateFitness(
                iputs=training_iputs,
                outputs=training_outputs
            )
            if individual.passed > highest_passed:
                highest_passed = individual.passed
            fitness += individual.fitness
            median_connections += len(individual.circuit.connections)
        median_connections = median_connections / len(self.currentGen)
        return fitness, highest_passed, median_connections

    def mutate(self, circuit):
        # Tries to increase genetic variation by creating new connections
        # Without a removal mutation to balance out this has a negative
        # impact on the performance of the algorithm
        # new_connection_chance = random.random()
        # if new_connection_chance >= self.mutation_chance:
        #     select_gate = random.randint(0, len(circuit.gates) - 1)

        #     if circuit.gates[select_gate].label == 'NOT':
        #         select_insert_point = random.randint(
        #             len(circuit.iput_conns), len(circuit.connections) - 1
        #         )
        #         # print("Issue is at not gate")
        #         # print(select_insert_point)
        #         select_pinA = random.randint(0, select_insert_point - 1)

        #         pinA = circuit.connections[select_pinA]
        #         gate = circuit.gates[select_gate]

        #         conn = Connection(
        #             pinA=pinA,
        #             pinB=None,
        #             gate=gate
        #         )
        #     else:
        #         select_insert_point = random.randint(
        #             len(circuit.iput_conns), len(circuit.connections) - 1
        #         )
        #         # print("Issue is at every other gate")
        #         # print(select_insert_point)
        #         select_pinA = random.randint(0, select_insert_point - 1)
        #         select_pinB = random.randint(0, select_insert_point - 1)

        #         pinA = circuit.connections[select_pinA]
        #         pinB = circuit.connections[select_pinB]
        #         gate = circuit.gates[select_gate]

        #         conn = Connection(
        #             pinA=pinA,
        #             pinB=pinB,
        #             gate=gate
        #         )

        #     # print(conn)
        #     circuit.connections.insert(select_insert_point, conn)
        #     circuit.generateMatrix()

        for i in range(len(circuit.connection_matrix)):
            matrix = circuit.connection_matrix[i]
            conn_index = matrix.connection_number
            pinA_index = matrix.pinA
            pinB_index = matrix.pinB
            gate_index = matrix.gate

            connection_mutation_chance = random.random()
            gate_mutation_chance = random.random()

            if connection_mutation_chance >= self.mutation_chance:
                if gate_mutation_chance >= self.mutation_chance:
                    gate_index = random.randint(0, len(circuit.gates) - 1)
                gate = circuit.gates[gate_index]
                pinA_index = random.randint(0, conn_index - 1)
                pinB_index = random.randint(0, conn_index - 1)
                if gate.label == 'NOT':
                    pinB_index = None

            circuit.connection_matrix[i] = ConnectionMatrix(
                connection_number=conn_index,
                pinA=pinA_index,
                pinB=pinB_index,
                gate=gate_index
            )

        circuit.reCreate()
        circuit.generateMap()

    def crossover(self, parentA, parentB):
        # if (len(parentA.circuit.connections) != len(parentB.circuit.connections)):
        #     raise AttributeError('Crossover is not viable')

        # Get details of parents
        pA_connections = parentA.circuit.connections
        pA_matrix = parentA.circuit.connection_matrix
        pA_iput_conns = parentA.circuit.iput_conns

        pB_connections = parentB.circuit.connections
        pB_matrix = parentB.circuit.connection_matrix
        pB_iput_conns = parentB.circuit.iput_conns

        # Select cross over point
        smaller_parent_con = None
        # print()
        # print('parent A: ' + str(len(pA_connections)))
        # print('parent B: ' + str(len(pB_connections)))
        if len(pA_connections) < len(pB_connections):
            smaller_parent_con = pA_connections
        else:
            smaller_parent_con = pB_connections

        # print('selected parent connection size: ' + str(len(smaller_parent_con)))
        # print()

        # print('parent A: ' + str(len(pA_matrix)))
        # print('parent B: ' + str(len(pB_matrix)))
        smaller_parent_matrix = None
        if len(pA_matrix) < len(pB_matrix):
            smaller_parent_matrix = pA_matrix
        else:
            smaller_parent_matrix = pB_matrix
        # print('selected parent matrix size: ' + str(len(smaller_parent_matrix)))

        crossover_point = random.randint(1, len(smaller_parent_matrix) - 1)
        # print('connection crossover point: ' + str(crossover_point))
        # Select parent connections to be inherited by child
        c1_select = pA_connections[:crossover_point] + pB_connections[crossover_point:]
        # print(c1_select)
        c2_select = pB_connections[:crossover_point] + pA_connections[crossover_point:]
        # print(c2_select)

        # Select which part of the connection matrix to inherit
        crossover_point = random.randint(0, len(smaller_parent_matrix) - 1)
        # print('matrix crossover point: ' + str(crossover_point))

        c1_matrix = pA_matrix[:crossover_point] + pB_matrix[crossover_point:]
        # print(c1_matrix)
        c2_matrix = pB_matrix[:crossover_point] + pA_matrix[crossover_point:]
        # print(c2_matrix)
        # Create fresh connections
        c1_conns = []
        c2_conns = []

        c1_iput_conns = []
        c2_iput_conns = []

        # Create iput connections
        for i in range(len(parentA.circuit.iput_conns)):
            c1_conn = Connection()
            c2_conn = Connection()

            c1_iput_conns.append(c1_conn)
            c1_conns.append(c1_conn)

            c2_iput_conns.append(c2_conn)
            c2_conns.append(c2_conn)

        # Generate new connection objects
        for i in range(len(c1_select)):
            # Check if connection is in the iput list
            if not ((c1_select[i] in pA_iput_conns) or (c1_select[i] in pB_iput_conns)):
                p_conn1 = c1_select[i]
                # p_conn2 = c2_select[i]

                c1_conn = Connection(gate=p_conn1.gate)
                c1_conns.append(c1_conn)

                # c2_conn = Connection(gate=p_conn2.gate)
                # c2_conns.append(c2_conn)

        for i in range(len(c2_select)):
            if not((c2_select[i] in pA_iput_conns) or (c2_select[i] in pB_iput_conns)):
                p_conn2 = c2_select[i]
                c2_conn = Connection(gate=p_conn2.gate)
                c2_conns.append(c2_conn)

        c1_circuit = Circuit(
            gates=parentA.circuit.gates,
            iput_conns=c1_iput_conns,
            connections=c1_conns,
            connection_matrix=c1_matrix
        )
        # Create circuit connections according to connection matrix
        c1_circuit.reCreate()
        self.mutate(c1_circuit)
        # Create connection map for circuit
        c1_circuit.generateMap()

        c2_circuit = Circuit(
            gates=parentB.circuit.gates,
            iput_conns=c2_iput_conns,
            connections=c2_conns,
            connection_matrix=c2_matrix
        )
        c2_circuit.reCreate()
        self.mutate(c2_circuit)
        c2_circuit.generateMap()

        s1 = Solution(circuit=c1_circuit)
        s2 = Solution(circuit=c2_circuit)

        return s1, s2

    # def


# Driver program to test functions
if __name__ == '__main__':
    print()
    inquire = [
        inquirer.List(
            'file',
            message='Select data set',
            choices=['data1.txt', 'data2.txt', 'data3.txt']
        )
    ]
    file_location = inquirer.prompt(inquire)

    try:
        file = open('../data-sets/' + file_location['file'], 'r')

        contents = []
        iput_set = []
        iputs = []
        outputs = []

        if file.mode == 'r':
            contents = file.readlines()

        # Ignore descriptions in the first line
        contents.pop(0)

        # Read File contents and seperate into inputs and outputs
        for content in contents:
            split_contents = content.split(" ")
            # Read for weird ass csv split file (turns out its not csv split)
            if len(split_contents) > 2:
                output = int(split_contents[-1])
                outputs.append(output)
                split_contents.pop(-1)
                # Normalise the data set
                iput_set = [round(float(x)) for x in split_contents]

                iputs.append(iput_set)
            # Read for normal sane files
            else:
                iput_set = split_contents[0]
                iput_set = [int(x) for x in iput_set]
                iputs.append(iput_set)
                outputs.append(int(split_contents[1]))

        # Generate training set
        if(len(iputs) > 100):
            training_set_iputs = iputs[:len(iputs) // 2]
            training_set_outputs = outputs[:len(outputs) // 2]

            # Generate test set
            test_set_iputs = iputs[len(iputs) // 2:]
            test_set_outputs = outputs[len(outputs) // 2:]
        else:
            training_set_iputs = iputs[:len(iputs) // 2]
            training_set_outputs = outputs[:len(outputs) // 2]

            # Generate test set
            test_set_iputs = iputs[len(iputs) // 2:]
            test_set_outputs = outputs[len(outputs) // 2:]

        print(f"training set size: {len(training_set_outputs)}")
        print(f'test set size: {len(test_set_outputs)}')

        ga = GeneticAlgorithm(
            max_gens=200,
            population=10,
            mutation_chance=0.4,
            selection='tournament'
        )

        (
            no_of_generations,
            fitnesses,
            gens,
            highest,
            median_connections
        ) = ga.start(
            training_iputs=training_set_iputs,
            training_outputs=training_set_outputs
        )

        print("No. of generations passed: " + str(no_of_generations))
        print()
        # print(ga.optimumSolution.circuit)
        # print('No. passed:' + str(ga.optimumSolution.passed))
        # print('Fitness: ' + str(ga.optimumSolution.fitness))
        print('Parent Gen\n========================')
        for individual in ga.parentGen:
            print('individual: ' + str(individual.circuit))
            print('fitness: ' + str(individual.fitness))
            print('Expected Outputs: ' + str(training_set_outputs))
            print('Generated Outputs: ' + str(individual.outputs))
            print('No of tests passed: ' + str(individual.passed))
            print()
        print('Final Gen\n========================')
        for individual in ga.finalGen:
            print('individual: ' + str(individual.circuit))
            print('fitness: ' + str(individual.fitness))
            print('Expected Outputs: ' + str(training_set_outputs))
            print('Generated Outputs: ' + str(individual.outputs))
            print('No of tests passed: ' + str(individual.passed))
            print()

        is_equal = 0
        for i in range(len(ga.finalGen) - 1):
            if str(ga.finalGen[i].circuit) + str(ga.finalGen[i + 1].circuit):
                is_equal += 1

        print(highest)

        fig1 = plt.figure()
        fig1.suptitle('Total fitness per generation')
        ax1 = fig1.add_subplot(1, 1, 1)
        ax1.set_xlabel('Generation')
        ax1.set_ylabel('Fitness')
        ax1.plot(gens, fitnesses, linestyle='-')

        fig2 = plt.figure()
        fig2.suptitle('Average number of connections')
        ax2 = fig2.add_subplot(1, 1, 1)
        ax2.set_xlabel('Generation')
        ax2.set_ylabel('Avg. Connections')
        ax2.plot(gens, median_connections, linestyle='-')

        plt.draw()
        plt.show()
    except Exception as e:
        traceback.print_exc()
