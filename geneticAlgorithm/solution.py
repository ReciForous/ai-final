import random
from circuit import (Circuit, ConnectionMatrix)
from connection import Connection
from logicGates import (
    AND,
    OR,
    NOT,
    XOR,
    NAND,
    NOR
)
from difflib import SequenceMatcher as sm


class Solution(object):
    def __init__(self, circuit=None):
        self.circuit = circuit
        self.outputs = []
        self.passed = 0
        self.simplicity = 0
        self.fitness = 0
        self.MAX_CONNECTIONS = 10

    def test(self, training_set):
        pass

    def generateCircuit(self, no_of_connections, iput_list):
        and_gate = AND()
        or_gate = OR()
        not_gate = NOT()
        xor_gate = XOR()
        nand_gate = NAND()
        nor_gate = NOR()

        gates = [and_gate, or_gate, not_gate, xor_gate, nand_gate, nor_gate]
        iput_conns = []
        connections = []
        # connection_mapper = []
        connection_matrix = []
        connection_number = 1

        # create input connections
        for iput in iput_list:
            iput_conn = Connection(pinO=iput)
            iput_conns.append(iput_conn)
            connections.append(iput_conn)

        # connect iput connections to gates
        for i in range(len(iput_conns)):
            gate = gates[random.randint(0, len(gates) - 1)]

            if gate.label == 'NOT':
                pinA = iput_conns[random.randint(0, len(iput_conns) - 1)]
                pinB = None
                connection = Connection(
                    pinA=pinA,
                    gate=gate
                )
                # mapper = f'connection{connection_number}: i{iput_conns.index(pinA) + 1} to {gate.label} gate'
            else:
                pinA = iput_conns[random.randint(0, len(iput_conns) - 1)]
                pinB = iput_conns[random.randint(0, len(iput_conns) - 1)]
                connection = Connection(
                    pinA=pinA,
                    pinB=pinB,
                    gate=gate
                )
                # mapper = f'connection{connection_number}: i{iput_conns.index(pinA) + 1} + i{iput_conns.index(pinB) + 1} to {gate.label} gate'
            # connection_mapper.append(mapper)
            connections.append(connection)
            new_conn = connections.index(connections[-1])
            matrix = ConnectionMatrix(
                connection_number=new_conn,
                pinA=connections.index(pinA),
                pinB=connections.index(pinB) if pinB else None,
                gate=gates.index(gate)
            )
            connection_matrix.append(matrix)
            no_of_connections -= 1
            connection_number += 1

        # create connection from other connections
        for i in range(no_of_connections):
            gate = gates[random.randint(0, len(gates) - 1)]

            if(gate.label == 'NOT'):
                pinA = connections[random.randint(0, len(connections) - 1)]
                pinB = None
                while not pinA.gate:
                    pinA = connections[random.randint(0, len(connections) - 1)]

                connection = Connection(pinA=pinA, gate=gate)
                # mapper = f'connection{connection_number}: connection{connections.index(pinA) + 1} to {gate.label} gate'
            else:
                pinA = connections[random.randint(0, len(connections) - 1)]
                pinB = connections[random.randint(0, len(connections) - 1)]

                while not (pinA.gate or pinB.gate):
                    pinA = connections[random.randint(0, len(connections) - 1)]
                    pinB = connections[random.randint(0, len(connections) - 1)]

                connection = Connection(pinA=pinA, pinB=pinB, gate=gate)
                # mapper = f'connection{connection_number}: connection{connections.index(pinA) + 1} + connection{connections.index(pinB) + 1} to {gate.label} gate'

            connections.append(connection)
            # connection_mapper.append(mapper)
            new_conn = connections.index(connections[-1])
            matrix = ConnectionMatrix(
                connection_number=new_conn,
                pinA=connections.index(pinA),
                pinB=connections.index(pinB) if pinB else None,
                gate=gates.index(gate)
            )
            connection_matrix.append(matrix)
            connection_number += 1

        gate = gates[random.randint(0, len(gates) - 1)]
        while(gate.label == 'NOT'):
            gate = gates[random.randint(0, len(gates) - 1)]

        pinA = connections[-1]
        pinB = connections[-2]
        output_conn = Connection(pinA=pinA, pinB=pinB, gate=gate)

        # mapper = f'connection{connection_number}: connection{connections.index(pinA) + 1} + connection{connections.index(pinB) + 1} to {gate.label} gate'
        connections.append(output_conn)
        # connection_mapper.append(mapper)
        new_conn = connections.index(connections[-1])
        matrix = ConnectionMatrix(
            connection_number=new_conn,
            pinA=connections.index(pinA),
            pinB=connections.index(pinB) if pinB else None,
            gate=gates.index(gate)
        )
        connection_matrix.append(matrix)

        self.circuit = Circuit(
            gates=gates,
            iput_conns=iput_conns,
            connections=connections,
            connection_matrix=connection_matrix
        )
        self.circuit.generateMap()
        return self.circuit

    def calculateFitness(self, iputs, outputs):
        size = len(iputs)

        no_1s = outputs.count(1)
        no_0s = outputs.count(0)

        passed_1s = 0
        passed_0s = 0
        self.passed = 0
        self.outputs = []
        for i in range(size):
            iput = iputs[i]
            expected_output = outputs[i]

            self.circuit.setIputs(iput_list=iput)
            actual_output = self.circuit.activate()
            self.outputs.append(actual_output)
            if actual_output == expected_output:
                self.passed += 1

        # Assess fitness by bitstring calculation (traditional)
        # passed = 0
        # total = 2 ** len(outputs)
        # for i in range(1, len(outputs) + 1):
        #     desired = outputs[-i]
        #     actual = self.outputs[-i]

        #     if actual == desired:
        #         passed += (2 ** (i - 1))

        # pass_percentage = passed / total

        # Assess fitness by bitstring calculation (modified)
        passed = 0
        total = 2 ** len(outputs)

        for i in range(len(outputs)):
            desired = outputs[i]
            actual = self.outputs[i]

            if actual == desired:
                passed += (2 ** i)

        pass_percentage = (passed / total) * 100

        # Asses fitness by checking list similarity
        # pass_percentage = sm(None, outputs, self.outputs).ratio()
        no_of_connections = len(self.circuit.connection_matrix)

        fitness = (pass_percentage * no_of_connections)
        # if(self.outputs.count(self.outputs[0]) == len(self.outputs)):
        #     fitness = 0

        # no_of_connections = len(self.circuit.connections)
        # self.simplicity = (no_of_connections / self.MAX_CONNECTIONS) * 100

        self.fitness = fitness
        return self.fitness


if __name__ == '__main__':
    iput_list = [1, 1, 1, 0, 1]
    solutions = []
    for i in range(3):
        solution = Solution()
        solution.generateCircuit(no_of_connections=5, iput_list=iput_list)
        solutions.append(solution)

    iputs = [[1, 1, 0, 1, 0], [0, 1, 1, 1, 1], [1, 1, 1, 1, 1], [0, 0, 0, 0, 0]]
    outputs = [1, 0, 1, 0]

    for solution in solutions:
        fitness = solution.calculateFitness(iputs=iputs, outputs=outputs)
        no_passed = solution.passed

        print('===========')
        print('Circuit: ' + str(solution.circuit))
        print('\nNo. of tests passed: ' + str(no_passed))
        print('\nFitness: ' + str(fitness))
        if no_passed == 4:
            print(solution.outputs)
