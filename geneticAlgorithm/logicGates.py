import numpy as np


class Gate(object):
    def __init__(self):
        self.output = None


class AND(Gate):
    def __init__(self):
        self.label = 'AND'
        self.output = None

    def __str__(self):
        return f'Gate(type=AND)'

    def __repr__(self):
        return str(self)

    def do(self, i1, i2):
        # self.output = np.bitwise_and.reduce([i1, i2])
        if(i1 == 1 and i2 == 1):
            self.output = 1
        else:
            self.output = 0
        return self.output


class OR(Gate):
    def __init__(self):
        self.label = 'OR'
        self.output = None

    def __str__(self):
        return f'Gate(type=OR, output={self.output})'

    def __repr__(self):
        return str(self)

    def do(self, i1, i2):
        # self.output = np.bitwise_or.reduce([i1, i2])
        if (i1 == 0 and i2 == 0):
            self.output = 0
        else:
            self.output = 1
        return self.output


class XOR(Gate):
    def __init__(self):
        self.label = 'XOR'
        self.output = None

    def __str__(self):
        return f'Gate(type=XOR, output={self.output})'

    def __repr__(self):
        return str(self)

    def do(self, i1, i2):
        # self.output = np.bitwise_xor.reduce([i1, i2])
        if((i1 == 1 and i2 == 1) or (i1 == 0 and i2 == 0)):
            self.output = 0
        else:
            self.output = 1
        return self.output


class NOT(Gate):
    def __init__(self):
        self.label = 'NOT'
        self.output = None

    def __str__(self):
        return f'Gate(type=NOT, output={self.output})'

    def __repr__(self):
        return str(self)

    def do(self, i1):
        if i1 == 1:
            self.output = 0
        else:
            self.output = 1
        return self.output


class NAND(Gate):
    def __init__(self):
        self.label = 'NAND'
        self.output = None

    def __str__(self):
        return f'Gate(type=NAND, output={self.output})'

    def __repr__(self):
        return str(self)

    def do(self, i1, i2):
        if(i1 == 1 and i2 == 1):
            self.output = 0
        else:
            self.output = 1
        return self.output


class NOR(Gate):
    def __init__(self):
        self.label = 'NOR'
        self.output = None

    def __str__(self):
        return f'Gate(type=NOR, output={self.output})'

    def do(self, i1, i2):
        if(i1 == 0 and i2 == 0):
            self.output = 1
        else:
            self.output = 0
        return self.output
