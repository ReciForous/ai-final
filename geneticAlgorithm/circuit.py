from connection import Connection
from logicGates import (
    AND,
    OR,
    XOR,
    NOT
)
import random
import collections

# Represents connections in the connections list for the circuit
ConnectionMatrix = collections.namedtuple(
    'ConnectionMatrix',
    'connection_number pinA pinB gate'
)


class Circuit():
    def __init__(
        self,
        gates=None,
        iput_conns=None,
        connections=None,
        connection_map=None,
        connection_matrix=None
    ):
        self.gates = gates
        self.connections = connections
        self.connection_map = connection_map
        self.iput_conns = iput_conns
        # Nested list with each list representing a connection in the form of
        # [connectionIndex, pinA, pinB, gate]
        self.connection_matrix = connection_matrix

    def __str__(self):
        return str(self.connection_map)

    def __repr__(self):
        return str(self)

    # Re Create circuit connections based on connection matrix
    def reCreate(self):
        for matrix in self.connection_matrix:
            if matrix.pinB is not None:
                self.connections[matrix.connection_number].edit(
                    pinA=self.connections[matrix.pinA],
                    pinB=self.connections[matrix.pinB],
                    gate=self.gates[matrix.gate]
                )
            else:
                self.connections[matrix.connection_number].edit(
                    pinA=self.connections[matrix.pinA],
                    gate=self.gates[matrix.gate]
                )

    def generateMap(self):
        self.connection_map = []
        for connection in self.connections:
            pinA = connection.pinA
            pinB = connection.pinB
            gate = connection.gate

            # Some useful debugging prints
            # print("connection index: " + str(self.connections.index(connection)))
            # print("pinA: " + str(pinA))
            # print("pinB: " + str(pinB))
            # print("gate: " + str(gate))
            # print()
            if not gate:
                mapper = f'c{self.connections.index(connection)}'
                self.connection_map.append(mapper)
            elif gate.label == 'NOT':
                mapper = f'c{self.connections.index(connection)}: c{self.connections.index(pinA)} to {gate.label} gate'
                self.connection_map.append(mapper)
            else:
                mapper = f'c{self.connections.index(connection)}: c{self.connections.index(pinA)} + c{self.connections.index(pinB)} to {gate.label} gate'
                self.connection_map.append(mapper)

    def generateMatrix(self):
        self.connection_matrix = []
        for connection in self.connections:
            # print(connection)
            if connection.gate:
                if connection.pinB:
                    matrix = ConnectionMatrix(
                        connection_number=self.connections.index(connection),
                        pinA=self.connections.index(connection.pinA),
                        pinB=self.connections.index(connection.pinB),
                        gate=self.gates.index(connection.gate)
                    )
                else:
                    matrix = ConnectionMatrix(
                        connection_number=self.connections.index(connection),
                        pinA=self.connections.index(connection.pinA),
                        pinB=None,
                        gate=self.gates.index(connection.gate)
                    )
                self.connection_matrix.append(matrix)

    def activate(self):
        if self.connections:
            for connection in self.connections:
                # if connection.gate.label == 'NOT':
                #     print(f'c{self.connections.index(connection) + 1}: {connection.pinA.pinO} to {connection.gate.label}')
                # else:
                #     print(f'c{self.connections.index(connection) + 1}: {connection.pinA.pinO} + {connection.pinB.pinO} to {connection.gate.label}')
                if connection.gate:
                    connection.activate()
                # print(f'output = {connection.pinO}')
                # print()

            output = self.connections[-1].pinO
            return output
        else:
            return 0

    # def getLooseConnections(self):
    #     loose_connections = []
    #     not_loose = True
    #     for matrix in connection_matrix:
    #         for connection in self.connections:
    #             pinA = matrix.pinA
    #             pinB = matrix.pinB

    #             conn_index = self.connections.index(connection)

    #             if not(pinA == conn_index or pinB == conn_index):

    def setIputs(self, iput_list):
        for iput in iput_list:
            self.iput_conns[iput_list.index(iput)].setIput(iput=iput)


if __name__ == '__main__':
    and_gate = AND()
    or_gate = OR()
    xor_gate = XOR()
    not_gate = NOT()

    iput1 = Connection(pinO=1)
    iput2 = Connection(pinO=1)
    iput3 = Connection(pinO=1)
    iput4 = Connection(pinO=1)
    iput5 = Connection(pinO=1)

    conn1 = Connection(pinA=iput2, pinB=iput2, gate=and_gate)
    conn2 = Connection(pinA=iput5, pinB=iput3, gate=xor_gate)
    conn3 = Connection(pinA=iput3, gate=not_gate)
    conn4 = Connection(pinA=iput1, pinB=iput4, gate=or_gate)
    connections = [conn1, conn2, conn3, conn4]
    conn5 = Connection(pinA=0, pinB=3, gate=and_gate, connections=connections)
    connections.append(conn5)
    conn6 = Connection(pinA=4, pinB=2, gate=and_gate, connections=connections)
    connections.append(conn6)

    conn_map = [
        'c1: i2 + i2 to AND gate',
        'c2: i5 + i3 to XOR gate',
        'c3: i3 to NOT gate',
        'c4: i1 + i4 to OR gate',
        'c5: c1 + c4 to AND gate',
        'c6: c5 + c3 to AND gate'
    ]

    circuit = Circuit(
        iput_conns=[iput1, iput2, iput3, iput4, iput5],
        connections=connections,
        connection_map=conn_map
    )
    output = circuit.activate()
    print(circuit)
    print()
    print(output)
