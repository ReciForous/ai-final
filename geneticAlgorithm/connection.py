from logicGates import (
    AND,
    OR,
    NOT,
    XOR
)


class Connection():
    def __init__(self, pinA=None, pinB=None, pinO=None, gate=None, connections=None):
        if not connections:
            self.pinA = pinA
            self.pinB = pinB
        else:
            self.pinA = connections[pinA]
            self.pinB = connections[pinB]
        self.pinO = pinO
        self.gate = gate

    def __str__(self):
        return f'Connection(pinA={self.pinA}, pinB={self.pinB}, pinO={self.pinO}, gate={self.gate})'

    def __repr__(self):
        return str(self)

    def activate(self):
        if self.gate.label == 'NOT':
            self.pinO = self.gate.do(self.pinA.pinO)
        else:
            self.pinO = self.gate.do(self.pinA.pinO, self.pinB.pinO)
        return self.pinO

    # This method is special for input connections
    def setIput(self, iput):
        self.pinO = iput

    def reset(self):
        self.pinA = None
        self.pinB = None
        self.gate = None
        self.pinO = None

    def edit(self, pinA=None, pinB=None, gate=None):
        self.pinA = pinA
        self.pinB = pinB
        self.gate = gate

