import random

class Expression(object):
    operations = [
        'and',
        'or',
        'not',
        '!=',
    ]
    MAXLENGTH = 12

    def __init__(self, expressionLength=5, expression=None):
        self.expressionLength = expressionLength
        self.expression = expression,
        self.expressions = []

    def create(self, varArr, expressionLength=None):
        if expressionLength:
            self.expressionLength = expressionLength
        self.expression = ''
        for i in range(self.expressionLength - 1):
            expressionType = random.randint(0, 4)
            varA = self.generateExpression(varArr, expressionType)
            expressionType = random.randint(0, 4)
            varB = self.generateExpression(varArr, expressionType)
            join1 = self.operations[random.randint(0, len(self.operations) - 1)]
            join2 = self.operations[random.randint(0, len(self.operations) - 1)]
            while(join1 == 'not' or join2 == 'not'):
                join1 = self.operations[random.randint(0, len(self.operations) - 1)]
                join2 = self.operations[random.randint(0, len(self.operations) - 1)]

            newExpr = varA + ' ' + join1 + ' ' + varB + ' ' + join2 + ' '
            self.expressions.append(newExpr)
            self.expression = self.expression + newExpr
            # print(expression)

        finalExpr = self.generateExpression(varArr, random.randint(0, 2))
        self.expressions.append(finalExpr)
        self.expression = self.expression + finalExpr
        return self.expression

    def recreate(self):
        self.expression = ''
        for expression in self.expressions:
            self.expression = self.expression + expression

        return self.expression

    def generateExpression(self, varArr, expressionType):
        varA = varArr[random.randint(0, len(varArr) - 1)]
        varB = varArr[random.randint(0, len(varArr) - 1)]

        while(varB == varA):
            varB = varArr[random.randint(0, len(varArr) - 1)]

        operator = self.operations[random.randint(0, len(self.operations) - 1)]
        while(operator == 'not'):
            operator = self.operations[random.randint(0, len(self.operations) - 1)]

        if expressionType == 0:
            return Expression.localExpression(varA, varB, operator)
        elif expressionType == 1:
            return Expression.globalExpression(varA, varB, operator)
        elif expressionType == 2:
            return Expression.notExpression(varA)
        elif expressionType == 3:
            return Expression.implicationExpression(varA, varB)
        else:
            return varA

    def expressionRules(expression, varArr):
        # Check if expression ends with a join
        for operation in self.operations:
            if expression.endsWith(operation):
                return False

        # Check if all variables are present in an expression
        for var in varArr:
            if not (f'{var} ' in expression):
                return False
        return True

    def localExpression(varA, varB, operator):
        return f'({varA} {operator} {varB})'

    def globalExpression(varA, varB, operator):
        return f'{varA} {operator} {varB}'

    def notExpression(varA):
        return f'(not {varA})'

    def implicationExpression(varA, varB):
        return f'((not {varA}) or {varB})'


if __name__ == '__main__':
    expression = Expression()
    print(expression.create(['a', 'b', 'c', 'd', 'e'], expressionLength=6))
