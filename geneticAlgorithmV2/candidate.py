import random
from expressionGenerator import Expression
from truths import Truths
from difflib import SequenceMatcher as sm


class Candidate(object):
    def __init__(self, labels, iputs, expectedOutputs, expressionLength=None, expression=None):
        self.equation = expression
        self.fitness = 0
        self.expressionLength = random.randint(2, 6)
        self.iputs = []
        self.labels = []
        self.outputs = []

        self.labels = labels
        if expressionLength:
            self.expressionLength = expressionLength
            self.equation.create(self.labels, self.expressionLength)
        else:
            self.equation.create(labels)

        self.iputs = iputs
        self.expectedOutputs = expectedOutputs

        self.testsPassed = 0
        # for output in self.expectedOutputs:
        #     self.testsPassed = (self.testsPassed << 1) | output

    def mutate(self):
        pass

    def calculateFitness(self):
        self.testsPassed = 0
        truth = Truths(
            iput_list=self.iputs,
            base=self.labels,
            phrases=[self.equation.expression]
        )
        truth.evaluate()
        self.outputs = truth.outputs

        # fullBit = []
        # for i in range(len(self.expectedOutputs)):
        #     val = 1
        #     fullBit.append(val)

        # fullBitVal = 0
        # for bit in fullBit:
        #     fullBitVal = (fullBitVal << 1) | bit

        # Convert to integer
        # actual = 0
        # for output in self.outputs:
        #     actual = (actual << 1) | output

        # print(actual)

        # # Convert to integer
        # expected = 0
        # for output in self.expectedOutputs:
        #     expected = (expected << 1) | output

        # Calculate fitness based on integer value
        # print(self.testsPassed)

        # similarity = abs(actual - self.testsPassed)
        # similarity = ((((fullBitVal - similarity) - expected)) / fullBitVal) * 100
        # print(similarity)

        for i in range(len(self.outputs)):
            if self.outputs[i] == self.expectedOutputs[i]:
                self.testsPassed += 1

        similarity = sm(None, self.expectedOutputs, self.outputs).ratio() * 100
        simplicity = (self.expressionLength / Expression.MAXLENGTH)
        # Fitness is modeled as
        self.fitness = (((self.testsPassed / len(self.expectedOutputs)) * 100) * (50 / 100)) + (similarity * (40 / 100)) + ((simplicity * 100) * 10 / 100)

if __name__ == '__main__':
    candidates = []
    iputs = [
        [0, 0, 0, 0, 1],
        [0, 0, 1, 1, 1],
        [0, 1, 1, 1, 0],
        [1, 0, 1, 0, 1],
        [1, 1, 0, 0, 0],
        [1, 1, 1, 1, 1]
    ]
    expectedOutputs = [0, 1, 0, 1, 1, 1]
    for i in range(5):
        candidate = Candidate(['a', 'b', 'c', 'd', 'e'], iputs, expectedOutputs, expression=Expression())
        print(len(candidate.equation.expressions))
        candidates.append(candidate)

    candidate = Candidate(['a', 'b', 'c', 'd', 'e'], iputs, expectedOutputs, expression=Expression())
    print(candidate.equation.expressions)
    # print(candidate.equation.expressions)
    print('')

    expression = Expression()
    expression.create(['a', 'b', 'c', 'd', 'e'])
    print(len(expression.expressions))
    print(expression.expressions)
    # print(expression.expression)
    # print('')

    candidate.equation.expressions = expression.expressions
    # print('')
    # print(candidate.equation.expression)
    # print('')
    # print(candidate.equation.expressions)
    print('')
    candidate.equation.recreate()
    print(len(candidate.equation.expressions))
    print(candidate.equation.expression)

    # [candidate.calculateFitness() for candidate in candidates]
    # for candidate in candidates:
    #     print(f'Logic equation: {candidate.equation.expression}')
    #     print(f'Length of equation: {candidate.expressionLength}')
    #     # candidate.calculateFitness()
    #     print(f'candidate fitness: {candidate.fitness}')
    #     print(f'Outputs: {candidate.outputs}')
    #     print(f'Expected Outputs: {candidate.expectedOutputs}')
    #     print('\n')
