# Repurposed code from Truths library from pypi:
# https://github.com/tr3buchet/truths/blob/master/setup.py#L2
# All credits for the library goes to Trey Morris (original author)
#
# Edited by Zain Riyaz

import itertools
from prettytable import PrettyTable
import re


class Gob(object):
    pass


class Truths(object):
    def __init__(self, iput_list, base=None, phrases=None, ints=True):
        if not base:
            raise Exception('Base items are required')

        self.base = base
        self.phrases = phrases or []
        self.ints = ints
        self.rows = []
        self.outputs = []

        # generate the sets of booleans for the bases
        # self.base_conditions = list(
        #     itertools.product(
        #         [False, True],
        #         repeat=len(base)
        #     )
        # )
        self.base_conditions = iput_list

        # print(self.base_conditions)

        # regex to match whole words defined in self.bases
        # used to add object context to variables in self.phrases
        self.p = re.compile(r'(?<!\w)(' + '|'.join(self.base) + ')(?!\w)')

    def calculate(self, *args):
        # store bases in an object context
        g = Gob()
        for a, b in zip(self.base, args):
            setattr(g, a, b)

        # add object context to any base variables in self.phrases
        # then evaluate each
        eval_phrases = []
        for item in self.phrases:
            item = self.p.sub(r'g.\1', item)
            ans = 1 if eval(item) else 0
            self.outputs.append(ans)
            eval_phrases.append(ans)

        # add the bases and evaluated phrases to create a single row
        row = [getattr(g, b) for b in self.base] + eval_phrases
        if self.ints:
            return [int(item) for item in row]
        else:
            return row

    def __str__(self):
        t = PrettyTable(self.base + self.phrases)
        # for conditions_set in self.base_conditions:
        #     t.add_row(self.calculate(*conditions_set))
        if(len(self.outputs) == 0):
            self.rows = self.evaluate()
        for row in self.rows:
            t.add_row(row)
        return str(t)

    def evaluate(self):
        self.outputs = []
        self.rows = []
        for conditions_set in self.base_conditions:
            row = self.calculate(*conditions_set)
            self.rows.append(row)
