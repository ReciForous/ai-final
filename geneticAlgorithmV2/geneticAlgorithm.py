# import truthtwo as ttw
import matplotlib.pyplot as plt
import pandas as pd
from expressionGenerator import Expression
from truths import Truths
import string
from candidate import Candidate
import random
import inquirer
import string
import time
# import ttg


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar.

    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


class GeneticAlgorithm(object):
    def __init__(
        self,
        populationSize,
        inputLabels,
        chromosomeSize=None,
        selectionAlgorithm='roulette',
        maxGens=2500,
        mutationChance=0.6
    ):
        self.populationSize = populationSize
        self.selectionAlgorithm = selectionAlgorithm
        if chromosomeSize:
            self.chromosomeSize = chromosomeSize
        self.maxGens = maxGens
        self.parentGen = []
        self.currentGen = []
        self.inputLabels = inputLabels
        self.mutationChance = mutationChance
        self.bestCandidate = None
        self.highestFitnesses = []

    # def generateParents(self, iputs, expectedOutputs):
    #     for i in range(self.populationSize):
    #         self.parentGen.append(
    #             Candidate(
    #                 self.inputLabels,
    #                 iputs,
    #                 expectedOutputs
    #             )
    #         )

    def rouletteWheel(self):
        totalPopFitness = 0
        probabilities = 0

        for candidate in self.currentGen:
            totalPopFitness += candidate.fitness

        relativeFitness = [candidate.fitness / totalPopFitness for candidate in self.currentGen]
        probabilities = [
            sum(relativeFitness[:i + 1]) for i in range(len(relativeFitness))
        ]

        selectedFitness = random.random()
        for (i, candidate) in enumerate(self.currentGen):
            if selectedFitness <= probabilities[i]:
                return candidate

    # Simple single point cross over method
    def crossover(self, parentA, parentB):
        parentAChrom = parentA.equation.expressions
        parentBChrom = parentB.equation.expressions

        if len(parentAChrom) <= len(parentBChrom):
            dominantChrom = parentAChrom
        else:
            dominantChrom = parentBChrom


        crossoverPoint = random.randint(0, len(dominantChrom) - 1)
        # print(parentAChrom[crossoverPoint:])
        # print(parentBChrom[:crossoverPoint])

        childAChrom = parentAChrom[:crossoverPoint] + parentBChrom[crossoverPoint:]
        childBChrom = parentBChrom[:crossoverPoint] + parentAChrom[crossoverPoint:]

        return childAChrom, childBChrom

    # Simple cingle point mutation
    def mutate(self, candidate):
        commitMutation = True if random.random() >= self.mutationChance else False

        if commitMutation:
            mutationPoint = random.randint(0, len(candidate.equation.expressions) - 1)
            mutatedExpression = candidate.equation.expression[mutationPoint]

            if mutationPoint != (len(candidate.equation.expressions) - 1):
                # print(mutationPoint)
                # print(len(candidate.equation.expressions) - 1)
                expression = Expression()
                expressionType = random.randint(0, 4)
                varA = expression.generateExpression(self.inputLabels, expressionType)
                expressionType = random.randint(0, 4)
                varB = expression.generateExpression(self.inputLabels, expressionType)
                join1 = Expression.operations[random.randint(0, len(Expression.operations) - 1)]
                join2 = Expression.operations[random.randint(0, len(Expression.operations) - 1)]

                while(join1 == 'not' or join2 == 'not'):
                    join1 = Expression.operations[random.randint(0, len(Expression.operations) - 1)]
                    join2 = Expression.operations[random.randint(0, len(Expression.operations) - 1)]
                # print(join1)
                # print(join2)
                mutatedExpression = f'{varA} {join1} {varB} {join2} '
            else:
                expression = Expression()
                expressionType = random.randint(0, 4)
                varA = expression.generateExpression(self.inputLabels, expressionType)
                expressionType = random.randint(0, 4)
                varB = expression.generateExpression(self.inputLabels, expressionType)
                join1 = Expression.operations[random.randint(0, len(Expression.operations) - 1)]

                while(join1 == 'not'):
                    join1 = Expression.operations[random.randint(0, len(Expression.operations) - 1)]
                    # join2 = Expression.operations[random.randint(0, len(Expression.operations) - 1)]
                mutatedExpression = f'{varA} {join1} {varB}'

            candidate.equation.expressions[mutationPoint] = mutatedExpression

    def start(self, trainingIputs, trainingOutputs):
        # Create parent generation
        for i in range(self.populationSize):
            self.parentGen.append(
                Candidate(
                    self.inputLabels,
                    trainingIputs,
                    trainingOutputs,
                    expression=Expression()
                )
            )

        self.currentGen = self.parentGen
        [candidate.calculateFitness() for candidate in self.currentGen]

        gen = 0
        perfectCandidate = False
        highestCandidateFitness = 0
        while(gen < self.maxGens and (not(perfectCandidate))):
            newGen = []
            while(len(newGen) < self.populationSize):
                parentA = self.rouletteWheel()
                parentB = self.rouletteWheel()

                childAChrom, childBChrom = self.crossover(parentA, parentB)
                # print(childAChrom)
                # print(childBChrom)

                childA = Candidate(self.inputLabels, trainingIputs, trainingOutputs, expression=Expression())
                childB = Candidate(self.inputLabels, trainingIputs, trainingOutputs, expression=Expression())

                childA.equation.expressions = childAChrom
                childB.equation.expressions = childBChrom

                childA.equation.recreate()
                childB.equation.recreate()

                childA.calculateFitness()
                childB.calculateFitness()

                newGen.append(childA)
                newGen.append(childB)

            # highestCandidateFitness = 0
            # Check fitness of new generation
            for candidate in newGen:
                if candidate.fitness > highestCandidateFitness:
                    self.bestCandidate = candidate
                    highestCandidateFitness = candidate.fitness
                if candidate.fitness > 85:
                    perfectCandidate = True
                    break

            if perfectCandidate:
                break

            #  Carry mutations on new generation
            for candidate in newGen:
                self.mutate(candidate)
                candidate.equation.recreate()
                candidate.calculateFitness()

            # Check if perfect candidate after mutation
            # Check fitness of new generation
            genHighest = 0
            for candidate in newGen:
                if candidate.fitness > highestCandidateFitness:
                    self.bestCandidate = candidate
                    highestCandidateFitness = candidate.fitness
                if candidate.fitness > genHighest:
                    genHighest = candidate.fitness
                    bestGenCandidate = candidate
                if candidate.fitness > 85:
                    perfectCandidate = True
                    break
            self.highestFitnesses.append(bestGenCandidate.fitness)
            if perfectCandidate:
                break

            self.currentGen = newGen
            printProgressBar(gen, self.maxGens - 1, prefix='Progress:', suffix='Complete', length=25)
            gen += 1

    def test(self, candidate, testIputs, testOutputs):
        truths = Truths(testIputs, base=self.inputLabels, phrases=[candidate.equation.expression])
        truths.evaluate()

        actual = truths.outputs
        expected = testOutputs

        print(f'Actual: {actual}')
        print(f'Expected: {expected}')
        print(truths)

        passed = 0

        for i in range(len(actual) - 1):
            if actual[i] == expected[i]:
                passed += 1

        return passed

if __name__ == '__main__':
    # trainingInputs = [
    #     [0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 1],
    #     [0, 0, 0, 1, 1],
    #     [1, 0, 1, 1, 1]
    # ]
    # trainingOutputs = [0, 0, 1, 1]
    inquire = [
        inquirer.List(
            'file',
            message='Select data set',
            choices=['data1.txt', 'data2.txt', 'data3.txt']
        )
    ]
    file_location = inquirer.prompt(inquire)

    try:
        file = open('../data-sets/' + file_location['file'], 'r')

        contents = []
        iput_set = []
        iputs = []
        outputs = []

        if file.mode == 'r':
            contents = file.readlines()

        # Ignore descriptions in the first line
        contents.pop(0)

        # Read File contents and seperate into inputs and outputs
        for content in contents:
            split_contents = content.split(" ")
            # Read for weird ass csv split file (turns out its not csv split)
            if len(split_contents) > 2:
                output = int(split_contents[-1])
                outputs.append(output)
                split_contents.pop(-1)
                # Normalise the data set
                iput_set = [round(float(x)) for x in split_contents]

                iputs.append(iput_set)
            # Read for normal sane files
            else:
                iput_set = split_contents[0]
                iput_set = [int(x) for x in iput_set]
                iputs.append(iput_set)
                outputs.append(int(split_contents[1]))

        # Generate training set
        if(len(iputs) > 100):
            trainingIputs = iputs[:len(iputs) // 2]
            trainingOutputs = outputs[:len(outputs) // 2]

            # Generate test set
            testIputs = iputs[len(iputs) // 2:]
            testOutputs = outputs[len(outputs) // 2:]
        else:
            trainingIputs = iputs[:len(iputs) // 2]
            trainingOutputs = outputs[:len(outputs) // 2]

            # Generate test set
            testIputs = iputs[len(iputs) // 2:]
            testOutputs = outputs[len(outputs) // 2:]

        print(f"training set size: {len(trainingOutputs)}")
        print(f'test set size: {len(testOutputs)}')
    except Exception as e:
        print(e)

    labels = list(string.ascii_lowercase)
    labels = labels[:len(trainingIputs[1])]
    print(f'labels: {labels}')
    ga = GeneticAlgorithm(
        10,
        ['a', 'b', 'c', 'd', 'e'],
        maxGens=250,
        mutationChance=0.7
    )

    start = time.time()
    ga.start(trainingIputs, trainingOutputs)
    end = time.time()
    # print(end - start)

    for candidate in ga.currentGen:
        print(f'Expression: {candidate.equation.expression}')
        print(f'Fitness: {candidate.fitness}')
        print('')

    candidate = ga.bestCandidate
    truths = Truths(trainingIputs, base=labels, phrases=[candidate.equation.expression])
    truths.evaluate()
    print(f'Actual: {candidate.outputs}')
    print(f'Expected: {candidate.expectedOutputs}')
    print(f'Fitness: {candidate.fitness}')
    print(f'Tests Passed: {candidate.testsPassed}')
    print(truths)

    print(f'Time taken to complete: {end - start}')

    print('Test Set Performance')
    testsPassed = ga.test(candidate, testIputs + trainingIputs, testOutputs + trainingOutputs)

    print(str(testsPassed) + '/' + str(len(testOutputs + trainingOutputs)))

    currentGenFitness = [candidate.fitness for candidate in ga.currentGen]
    parentGenFitness = [candidate.fitness for candidate in ga.parentGen]
    individuals = []
    for i in range(len(parentGenFitness)):
        individuals.append(i)

    fitnesses = ga.highestFitnesses
    # print(fitnesses)
    gens = []
    for i in range(len(fitnesses)):
        gens.append(i)

    fig1 = plt.figure()
    fig1.suptitle('Total fitness per generation')
    ax1 = fig1.add_subplot(1, 1, 1)
    ax1.set_xlabel('Generation')
    ax1.set_ylabel('Highest Fitness / Generation')
    ax1.plot(gens, fitnesses, linestyle='-')

    fig2 = plt.figure()
    fig2.suptitle('Individual Fitnesses for Final Gen vs Parent Gen')
    ax2 = fig2.add_subplot(1, 1, 1)
    ax2.set_xlabel('Individual No.')
    ax2.set_ylabel('Fitness')
    ax2.plot(individuals, currentGenFitness, linestyle='-', label='Current Gen')
    ax2.plot(individuals, parentGenFitness, linestyle='-', label='Parent Gen')

    plt.draw()
    plt.show()
