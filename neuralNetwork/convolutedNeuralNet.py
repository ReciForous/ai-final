from network import Network, printProgressBar
import inquirer
import crayons
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


def main():
    """Driver program for testing out network class."""
    print()
    inquire = [
        inquirer.List(
            'file',
            message='Select data set',
            choices=['data1.txt', 'data2.txt', 'data3.txt']
        )
    ]
    file_location = inquirer.prompt(inquire)

    inquire = [
        inquirer.List(
            'activation_function',
            message='Select activation function of neurons',
            choices=['sigmoid', 'step', 'htan']
        )
    ]

    activation_function = inquirer.prompt(inquire)['activation_function']
    while(True):
            try:
                learning_rate = float(input('Enter learning rate: '))

                break
            except Exception as e:
                print(f'Error: {e}')

    while(True):
            try:
                max_epochs = int(input('Enter max time to train: '))

                break
            except Exception as e:
                print(f'Error: {e}')

    try:
        file = open('../data-sets/' + file_location['file'], 'r')

        contents = []
        input_set = []
        inputs = []
        outputs = []

        sqe_plot = []
        epoch_plot = []

        if file.mode == 'r':
            contents = file.readlines()

        # Ignore descriptions in the first line
        contents.pop(0)

        # Read File contents and seperate into inputs and outputs
        for content in contents:
            split_contents = content.split(" ")
            # Read for weird ass csv split file (turns out its not csv split)
            if len(split_contents) > 2:
                output = int(split_contents[-1])
                outputs.append(output)
                split_contents.pop(-1)
                # Normalise the data set
                input_set = [round(float(x)) for x in split_contents]

                inputs.append(input_set)
            # Read for normal sane files
            else:
                input_set = split_contents[0]
                input_set = [int(x) for x in input_set]
                inputs.append(input_set)
                outputs.append(int(split_contents[1]))

        # divide contents between training and test sets
        if(len(inputs) > 100):
            training_set_inputs = inputs[:len(inputs) // 8]
            training_set_outputs = outputs[:len(outputs) // 8]

            # Generate test set
            test_set_inputs = inputs[len(inputs) // 8:]
            test_set_outputs = outputs[len(outputs) // 8:]
        else:
            training_set_inputs = inputs[:len(inputs) // 6]
            training_set_outputs = outputs[:len(outputs) // 6]

            # Generate test set
            test_set_inputs = inputs[len(inputs) // 6:]
            test_set_outputs = outputs[len(outputs) // 6:]

        print(f"training set size: {len(training_set_outputs)}")
        print(f'test set size: {len(test_set_outputs)}')

        # no_inputs = input("Enter number of input neurons: ")
        # no_output_neurons = input("Enter number of output neurons: ")
        # no_layers = input("Enter number of layers: ")

        network = Network(
            no_iputs=len(training_set_inputs[0]),
            no_output_neurons=1,
            no_layers=3,
            learning_rate=learning_rate,
            activation_function=activation_function
        )

        network.initialize()
        network.details()

        print('-----------------------')
        print(crayons.yellow('Training network'))
        print('-----------------------')
        sqes, epochs = network.train(training_set_inputs, training_set_outputs, max_epochs=max_epochs)

        # display network before training and after training
        network.compare()
        test_sqe = []
        for i in range(max_epochs):
            printProgressBar(i, max_epochs, prefix='Progress:', suffix='Complete', length=25)
            sqe, number_correct = network.test(test_set_inputs, test_set_outputs)
            test_sqe.append(sqe)

        print(f'Number correct: {number_correct}')
        print(f'% correct: {(number_correct) / len(test_set_outputs) * 100}')

        fig = plt.figure()
        fig.suptitle('Network training')
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlabel('Epoch')
        ax.set_ylabel('Sum of squared errors')
        ax.plot(epochs, sqes, label='training')
        ax.plot(epochs, test_sqe, label='test')
        ax.legend()

        # fig1 = plt.figure()
        # fig1.suptitle('Network performance')
        # ax1 = fig1.add_subplot(1, 1, 1)
        # ax1.set_xlabel('Weighed sum of inputs')
        # ax1.set_ylabel('Output')
        # ax1.plot(weighed_sums, actual_outputs, linestyle='-', label='actual output')
        # # ax1.plot(weighed_sums, test_set_outputs, linestyle='dashed', label='desired output')
        # ax1.legend()

        print('Displaying network results...')
        plt.draw()
        plt.show()
    except FileNotFoundError as e:
        print('File not found, Please make sure data set files are in data set folder!')


if __name__ == '__main__':
    main()
