import numpy as np


class Neuron(object):
    def __init__(
        self,
        no_of_connections=None,
        input_layer=False,
        output_layer=False,
        activation_function='sigmoid'
    ):
        self.iput = None
        self.threshold = None
        self.weights = None
        self.activation_function = activation_function
        if not output_layer:
            self.weights = []
            if no_of_connections:
                self.weights = np.random.uniform(size=no_of_connections)
        if not input_layer:
            self.threshold = np.random.uniform()
        self.output = None
        self.error_gradient = None

    def __str__(self):
        return f'Neuron(weights={self.weights}, threshold={self.threshold}, output={self.output})'

    def __repr__(self):
        return f'Neuron(weights={self.weights}, threshold={self.threshold}, output={self.output})'

    def activate(self, weighed_iput_sum):
        if self.activation_function == 'sigmoid':
            self.output = Neuron.sigmoid(weighed_iput_sum)
            return self.output
        elif self.activation_function == 'step':
            self.output = Neuron.step(weighed_iput_sum)
            return self.output
        elif self.activation_function == 'htan':
            self.output = Neuron.htan(weighed_iput_sum)
            return self.output
        else:
            raise AttributeError('No such activation function')

    def calculate_error_grad(self, error=None, prev_error_grad=None, learning_rate=None):
        error_gradient = 0
        if (prev_error_grad):
            error_gradient = self.output * (1 - self.output) * prev_error_grad
        if error:
            error_gradient = self.output * (1 - self.output) * error
        error_gradient = round(error_gradient, 5)
        self.error_gradient = error_gradient

        if self.threshold:
            self.threshold = (
                self.threshold + (learning_rate * -1 * self.error_gradient)
            )
        return error_gradient

    def learn(self, error_gradient, learning_rate, weight_set):
        self.weights[weight_set] = (
            self.weights[weight_set] +
            (learning_rate * self.iput * error_gradient)
        )

    def sigmoid(weighed_iput_sum):
        return 1 / (1 + np.exp(- weighed_iput_sum))

    def step(weighed_iput_sum):
        if weighed_iput_sum > 0:
            return 1
        else:
            return 0

    def htan(weighed_iput_sum):
        return np.tanh(weighed_iput_sum)
