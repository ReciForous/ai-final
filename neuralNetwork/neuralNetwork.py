"""
Summary: A multilayered neural network with scalabel number of hidden neurons.

Author: Zain Riyaz (S1700752)

Description
--------------------------------------------------------------------
Multi layered neural network for classifying a set of inputs as either
0 or 1, inputs are read from the provided data files (data1.txt, data2.txt
data3.txt)
--------------------------------------------------------------------
"""

import random
import numpy as np
import math
import crayons
import inquirer
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import multiprocessing as mp


class Neuron:
    """
    Summary: Basic neuron structure of the network.

    properties:
        inputs - list of inputs fed to neuron
        weights - list of weights associated with a neuron
        outputs - output generated by the neuron
        threshold - value for neuron to produce an output
        prev_weight_adjustments - list required for weight correction
        when using momentum
        error - error generated by a neuron

    methods:
        init_weight(weight) - used to store randomly generated
        weight for each connection
        set_weight(weight_set, weight) - used for assigning corrected weight
        set_threshold(threshold) - set threshold value after correction
        learn(desired_output, learning_rate, error_gradient, momentum) -
        adjusts weights and thresholds of a neuron
        generate_output(input_set, weight_set) - generates output using a
        sigmoid activation function
    """

    def __init__(self, inputs=None, outputs=None):
        self.inputs = inputs
        self.weights = []
        self.threshold = round(random.random(), 5)
        self.outputs = outputs
        self.prev_weight_adjustments = []

    def __repr__(self):
        return(
            f'Neuron(' +
            f'inputs={self.inputs}, ' +
            f'weights={self.weights}, ' +
            f'threshold={self.threshold}, ' +
            f'outputs={self.outputs})'
        )

    def init_weight(self, weight=None):
        if weight:
            self.weights.append(weight)

    def set_weight(self, weight_set=None, weight=None):
        self.weights[weight_set] = weight

    def set_inputs(self, inputs=None):
        self.inputs = inputs

    def set_threshold(self, threshold=None):
        self.threshold = threshold

    # inputs and weights should be a list from the previous layer
    def generate_output(self, inputs, weights, bias_present=False):
        sum_weighed_inputs = 0
        if bias_present:
            bias = 1
        else:
            bias = 0
        # calculate sum of weighed inputs
        for i in range(len(inputs)):
            sum_weighed_inputs = sum_weighed_inputs + round((inputs[i] * weights[i]), 5) + bias
        sum_weighed_inputs = sum_weighed_inputs - self.threshold

        # run through a sigmoid function
        self.outputs = round((1 / (1 + math.exp(-(sum_weighed_inputs)))), 5)


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar.

    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def main():
    print()
    inquire = [
        inquirer.List(
            'file',
            message='Select data set',
            choices=['data1.txt', 'data2.txt', 'data3.txt']
        )
    ]
    file_location = inquirer.prompt(inquire)

    try:
        file = open('../data-sets/' + file_location['file'], 'r')

        contents = []
        input_set = []
        inputs = []
        no_of_neurons = 0
        no_of_hidden_layer_neurons = 0
        outputs = []

        input_neurons = []
        hidden_neurons = []
        sqe_plot = []
        epoch_plot = []

        if file.mode == 'r':
            contents = file.readlines()

        # Ignore descriptions in the first line
        contents.pop(0)

        # Read File contents and seperate into inputs and outputs
        for content in contents:
            split_contents = content.split(" ")
            # Read for weird ass csv split file (turns out its not csv split)
            if len(split_contents) > 2:
                output = int(split_contents[-1])
                outputs.append(output)
                split_contents.pop(-1)
                # Normalise the data set
                input_set = [round(float(x)) for x in split_contents]

                inputs.append(input_set)
            # Read for normal sane files
            else:
                input_set = split_contents[0]
                input_set = [int(x) for x in input_set]
                inputs.append(input_set)
                outputs.append(int(split_contents[1]))

        # Generate training set
        if(len(inputs) > 100):
            training_set_inputs = inputs[:len(inputs) // 8]
            training_set_outputs = outputs[:len(outputs) // 8]

            # Generate test set
            test_set_inputs = inputs[len(inputs) // 8:]
            test_set_outputs = outputs[len(outputs) // 8:]
        else:
            training_set_inputs = inputs[:len(inputs) // 6]
            training_set_outputs = outputs[:len(outputs) // 6]

            # Generate test set
            test_set_inputs = inputs[len(inputs) // 6:]
            test_set_outputs = outputs[len(outputs) // 6:]

        print(f"training set size: {len(training_set_outputs)}")
        print(f'test set size: {len(test_set_outputs)}')

        # print(f'{crayons.red("Training set")}: \ninputs\n{training_set_inputs}\n\noutputs\n{training_set_outputs}\n')
        # print(f'{crayons.red("Test set")}: \ninputs\n{test_set_inputs}\n\noutputs\n{test_set_outputs}\n')

        # Set number of input neurons based on the file contents
        no_of_neurons = len(input_set)

        # Set number of hidden layer neurons based on half the number of input neurons and output neurons
        # allowing this to be dynamic
        # Suggested no. of hidden layer neurons = sum of input neurons +
        #sum of output neurons / 2
        no_of_hidden_layer_neurons = math.ceil((no_of_neurons + 1) / + 2)

        print(f'No. of neurons: {no_of_neurons}')
        print(f'No. of hidden layer neurons: {no_of_hidden_layer_neurons}')

        # Initialize neurons
        for i in range(no_of_neurons):
            input_neuron = Neuron()
            input_neurons.append(input_neuron)

        for i in range(no_of_hidden_layer_neurons):
            hidden_neuron = Neuron()
            hidden_neurons.append(hidden_neuron)

        output_neuron = Neuron()

        # learning_rate = round(
        #     (
        #         ((no_of_neurons + no_of_hidden_layer_neurons) / 100) *
        #         (no_of_hidden_layer_neurons + no_of_neurons / 2)
        #     ), 1
        # )

        learning_rate = 0.42
        # learning_rate = 0.9

        # Sum of squared errors
        sqe = 1

        print(f'Learning rate: {learning_rate}')

        # weight initialization for neurons
        for input_neuron in input_neurons:
            for i in range(no_of_hidden_layer_neurons):
                weight = round(random.random(), 5)
                input_neuron.init_weight(weight)

        for hidden_neuron in hidden_neurons:
            hidden_neuron.init_weight(weight=round(random.random(), 5))

        print()
        print('-----------------------')
        print(crayons.green('Initial weights and thresholds'))
        print('-----------------------')
        print(crayons.cyan('Input layer', bold=True))
        for input_neuron in input_neurons:
            print(f'    Neuron {input_neurons.index(input_neuron) + 1} - {input_neuron.weights}')

        print(crayons.cyan('\nHidden layer', bold=True))
        for hidden_neuron in hidden_neurons:
            print(f'    Hidden Neuron {hidden_neurons.index(hidden_neuron) + 1} - {hidden_neuron.weights}, {hidden_neuron.threshold}')

        print(crayons.cyan('\nOutput layer', bold=True))
        print(f'    Output Neuron - {output_neuron.weights}, {output_neuron.threshold}')
        # Need to insert training logic here
        print()
        print('-----------------------')
        print(crayons.yellow('Training network'))
        print('-----------------------')
        epoch = 0
        bar_progress = 0
        # while epoch <= 200:
        while sqe > 0.001:
            sqe = 0
            printProgressBar(bar_progress, 20000, prefix='Progress:', suffix='Complete', length=25)
            # start iterating through the training set
            for i in range(len(training_set_inputs)):
                input_set = training_set_inputs[i]
                desired_output = training_set_outputs[i]
                # assign inputs to input layer
                for j in range(len(input_set)):
                    input_neurons[j].set_inputs(inputs=input_set[j])

                # pass input from input layer to hidden_neurons and generate
                # hidden layer outputs
                for j in range(no_of_hidden_layer_neurons):
                    iput_layer_iput = [input_neuron.inputs for input_neuron in input_neurons]
                    iput_layer_weight = []
                    for input_neuron in input_neurons:
                        iput_layer_weight.append(input_neuron.weights[j])
                    hidden_neurons[j].generate_output(
                        inputs=iput_layer_iput,
                        weights=iput_layer_weight
                    )
                    # This part is only done to keep the .learn method for neurons same
                    hidden_neurons[j].set_inputs(inputs=hidden_neurons[j].outputs)

                # get weights from hidden layer and pass to output neuron with inputs
                # (output of hidden layer is input for output layer)
                hidden_layer_weights = []
                hidden_layer_outputs = []
                for hidden_neuron in hidden_neurons:
                    hidden_layer_weights.append(hidden_neuron.weights[0])
                    hidden_layer_outputs.append(hidden_neuron.outputs)

                output_neuron.generate_output(
                    inputs=hidden_layer_outputs,
                    weights=hidden_layer_weights
                )

                # calculate error gradient for output layer
                error = desired_output - output_neuron.outputs
                error_grad_o = output_neuron.outputs * (1 - output_neuron.outputs) * error

                # calculate error gradient for hidden layer
                error_gradients = []
                for hidden_neuron in hidden_neurons:
                    error_gradients.append(
                        round(
                            (
                                hidden_neuron.outputs *
                                (1 - hidden_neuron.outputs) *
                                (error_grad_o * hidden_neuron.weights[0])
                            ), 5
                        )
                    )

                # calculate and set threshold value for output neuron
                output_neuron.set_threshold(round(
                    (
                        output_neuron.threshold + (learning_rate * (-1) * error_grad_o)
                    ),
                    5)
                )
                for j in range(no_of_hidden_layer_neurons):
                    # calculate weight for hidden layer neuron and adjust
                    hidden_neurons[j].set_weight(
                        weight_set=0,
                        weight=round(
                            (
                                hidden_neurons[j].weights[0] +
                                (
                                    learning_rate * hidden_neurons[j].inputs *
                                    error_grad_o
                                )
                            ),
                            5
                        )
                    )
                    # calculate and set thresholds for hidden neurons
                    hidden_neurons[j].set_threshold(
                        round(
                            (
                                hidden_neurons[j].threshold +
                                (learning_rate * (-1) * error_gradients[j])
                            ), 5
                        )
                    )
                    # adjust weights and
                    for input_neuron in input_neurons:
                        input_neuron.set_weight(
                            weight_set=j,
                            weight=round(
                                (
                                    input_neuron.weights[j] +
                                    (
                                        learning_rate * input_neuron.inputs *
                                        error_gradients[j]
                                    )
                                ),
                                5
                            )
                        )
                for hidden_neuron in hidden_neurons:
                    hidden_neuron.prev_weight_adjustments.append(hidden_neuron.weights)

                for input_neuron in input_neurons:
                    input_neuron.prev_weight_adjustments.append(input_neuron.weights)

                sqe = round(sqe + (error * error), 5)
            sqe_plot.append(sqe)
            epoch_plot.append(epoch)
            epoch += 1
            bar_progress += 1
            if sqe <= 0.001:
                bar_progress = 20000
            printProgressBar(bar_progress, 20000, prefix='Progress:', suffix='Complete', length=25)
            if epoch == 20000:
                # print('Epochs exceeded')
                break

        print()
        print(f'Network trained for {epoch} epochs')
        print(f'Sum of squared errors: {sqe}\n')

        # Show plotting for network training here
        # showGraph = mp.Process(target=displayNetworkTraining, args=(
        #     {'x': epoch_plot, 'y': sqe_plot})
        # )
        fig1 = plt.figure()
        fig1.suptitle(f'Network training\nLearning Rate: {learning_rate}')
        ax1 = fig1.add_subplot(1, 1, 1)
        ax1.set_xlabel('Epochs')
        ax1.set_ylabel('Sum of squared errors')
        ax1.plot(epoch_plot, sqe_plot, linestyle='-')

        print('-----------------------')
        print(crayons.green('Final weights and thresholds'))
        print('-----------------------')
        print(crayons.cyan('Input layer', bold=True))
        for input_neuron in input_neurons:
            print(f'    Neuron {input_neurons.index(input_neuron) + 1} - {input_neuron.weights}')

        print(crayons.cyan('\nHidden layer', bold=True))
        for hidden_neuron in hidden_neurons:
            print(f'    Hidden Neuron {hidden_neurons.index(hidden_neuron) + 1} - {hidden_neuron.weights}, {hidden_neuron.threshold}')

        print(crayons.cyan('\nOutput layer', bold=True))
        print(f'    Output Neuron - {output_neuron.weights}, {output_neuron.threshold}')
        # Its big brain time...(Uncomment this when its time to test)
        # start iterating through the test set
        correctly_classified = 0
        print('\n-----------------------')
        print(crayons.yellow('Testing network with test set'))
        print('-----------------------')

        test_set_plot = []
        actual_result_plot = []
        expected_result_plot = []
        actual_expected_plot = []

        for i in range(len(test_set_inputs)):
            test_set_plot.append(i)
            input_set = test_set_inputs[i]
            desired_output = test_set_outputs[i]
            expected_result_plot.append(desired_output)
            # assign inputs to input layer
            for j in range(len(input_set)):
                input_neurons[j].set_inputs(inputs=input_set[j])

            # pass input from input layer to hidden_neurons and generate
            # hidden layer outputs
            for j in range(no_of_hidden_layer_neurons):
                iput_layer_iput = [input_neuron.inputs for input_neuron in input_neurons]
                iput_layer_weight = []
                for input_neuron in input_neurons:
                    iput_layer_weight.append(input_neuron.weights[j])
                hidden_neurons[j].generate_output(
                    inputs=iput_layer_iput,
                    weights=iput_layer_weight
                )

            # get weights from hidden layer and pass to output neuron with inputs
            # (output of hidden layer is input for output layer)
            hidden_layer_weights = []
            hidden_layer_outputs = []
            for hidden_neuron in hidden_neurons:
                hidden_layer_weights.append(hidden_neuron.weights[0])
                hidden_layer_outputs.append(hidden_neuron.outputs)

            output_neuron.generate_output(
                inputs=hidden_layer_outputs,
                weights=hidden_layer_weights
            )
            # difference = abs(desired_output - output_neuron.outputs)

            actual_result_plot.append(output_neuron.outputs)
            # actual_expected_plot.append(difference)

            if round(output_neuron.outputs) == desired_output:
                correctly_classified += 1

        percentage_correct = round((correctly_classified / len(test_set_outputs) * 100), 2)
        print(f'Number Correct: {correctly_classified}/{len(test_set_outputs)}')
        print('% correct: ' + str(percentage_correct) + '%\n')

        # Plot second figure - expected result vs actual result against training set
        fig2 = plt.figure()
        fig2.suptitle(f'Network performance\nNo. Correct - {correctly_classified}/{len(test_set_outputs)}\n% Correct - {percentage_correct}%')
        ax2 = fig2.add_subplot(1, 1, 1)
        ax2.set_xlabel('Output')
        ax2.set_ylabel('Test set No.')
        ax2.scatter(actual_result_plot, test_set_plot, marker='o', label='actual result')
        ax2.scatter(expected_result_plot, test_set_plot, marker='x', label='expected result')
        # ax2.plot(actual_expected_plot, test_set_plot, linestyle='dashed', label='output difference', color='limegreen')
        ax2.legend()

        print('Displaying network results...')
        plt.style.use('seaborn-darkgrid')
        plt.draw()
        plt.show()
    except FileNotFoundError as e:
        print('File not found, Please make sure data set files are in data set folder!')


if __name__ == '__main__':
    main()
