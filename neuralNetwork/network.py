import math
from neuron import Neuron
import random
import numpy as np
import math
import crayons
import copy
import pprint


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar.

    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


class Network(object):
    """
    Neural network object which can scale to N number of input neurons, N number of hidden layers and N number of output neurons.

    @attributes:
        no_iputs            - Required  : number of input neurons in the input layer (Int)
        no_output_neurons   - Required: number of output neurons in output layer (Int)
        no_layers           - Required: number of hidden layers (Int)
        learning_rate       - Required: learning rate of the network (Float)
        activation_function - Required: activation function used by neurons in network (Str)
        iput_neurons        - Auto    : List of Neuron objects for input layer (list)
        output_neurons      - Auto    : List of Neuron objects for output layer (list)
        layers              - Auto    : Nested list of hidden neurons in each layer (list)
        old                 - Auto    : Deepcopy of network during intialization (Network)
    """

    def __init__(
        self,
        no_iputs,
        no_output_neurons,
        no_layers,
        learning_rate,
        activation_function
    ):
        """
        Constructer method for class.

        @params:
            no_iputs    - Required: no. of input neurons (int)
            no_output_neurons   - Required: no. of output neurons (int)
            no_layers           - Required: no. of layers (int)
            learning_rate       - Required: learning rate for network(float)
            activation_function - Required: activation function of neurons (str)
        """
        self.no_iputs = no_iputs
        self.no_output_neurons = no_output_neurons
        self.no_hidden_neurons = math.ceil((no_iputs + no_output_neurons) / 2)
        self.learning_rate = learning_rate
        self.no_layers = no_layers

        max_layers = math.floor(no_iputs / 2)

        # Good reason to cap this because logic flaw, dont feel like fixing it
        if self.no_layers > max_layers:
            self.no_layers = max_layers

        self.iput_neurons = []
        self.output_neurons = []
        self.layers = []
        self.old = None
        self.activation_function = activation_function

    # def __str__(self):
    #     return (
    #         f'No of inputs: {self.no_iputs}\n' +
    #         f'No of outputs neurons: {self.no_output_neurons}\n' +
    #         f'No of hidden neurons: {self.no_hidden_neurons}\n' +
    #         f'No of hidden layers : {self.no_layers}\n\n' +
    #         'Input Neurons: \n' +
    #         f'{self.iput_neurons}' +
    #         '\nHidden neurons: ')

    def __str__(self):
        """Represent network as string."""
        rv = {
            'input neurons': self.iput_neurons,
            'hidden layers': self.layers,
            'output neurons': self.output_neurons,
            'activation function': self.activation_function,
            'learning rate': self.learning_rate
        }

        return pprint.pformat(rv)

    def __repr__(self):
        """Repr method for network."""
        return str(self)

    # create neurons and initialize weights and thresholds for relevant neurons
    def initialize(self):
        """
        Populate network with neurons.

        @params:
            self
        """
        for i in range(self.no_iputs):
                neuron = Neuron(
                    no_of_connections=self.no_hidden_neurons,
                    input_layer=True,
                    activation_function=self.activation_function
                )
                # print(neuron)
                self.iput_neurons.append(neuron)

        for i in range(self.no_layers):
            layer = []
            neurons_in_layer = self.no_hidden_neurons - i
            for j in range(neurons_in_layer):
                if(i == self.no_layers - 1):
                    neuron = Neuron(
                        no_of_connections=self.no_output_neurons,
                        activation_function=self.activation_function
                    )
                else:
                    neuron = Neuron(
                        no_of_connections=neurons_in_layer - 1,
                        activation_function=self.activation_function)
                layer.append(neuron)
            self.layers.append(layer)

        for i in range(self.no_output_neurons):
            neuron = Neuron(
                output_layer=True,
                activation_function=self.activation_function)
            self.output_neurons.append(neuron)

        self.old = copy.deepcopy(self)

    # assign inputs to the input layer
    def assign_inputs(self, iput_set):
        """
        Assign inputs to input layer.

        @params:
            self
            iput_set    - Required: list of inputs(list)
        """
        for i in range(self.no_iputs):
            self.iput_neurons[i].iput = iput_set[i]

    # Summation function of network
    def summation(neurons=None, weight_set=None, threshold=None):
        """
        Calculate weighed sum of inputs for a given list of neurons and weight set.

        @params:
            neruons    - Required: list of neurons to be taken as input (list)
            weight_set - Required: index of weight set to be used (int)
            threshold  - Required: threshold of neuron (float)
        """
        if not(neurons or weight_set or threshold):
            raise AttributeError("Missing neurons or weight set for summation")

        summation = 0
        # calculate sum of weighed inputs for a layer of neurons
        for neuron in neurons:
            summation = summation + (neuron.iput * neuron.weights[weight_set])
        summation = summation - threshold

        # return sum of weighed inputs
        return summation

    # predict output for a provided input set
    def predict(self, iput_set):
        """
        Predict output for a given set of inputs.

        @params:
            self
            iput_set - Required: list of inputs (list)
        """
        self.assign_inputs(iput_set)
        layer = self.layers[0]

        # calculate output for the first hidden layer

        for hidden_neuron in layer:
            weighed_sum = Network.summation(
                neurons=self.iput_neurons,
                weight_set=layer.index(hidden_neuron),
                threshold=hidden_neuron.threshold
            )
            output = hidden_neuron.activate(weighed_iput_sum=weighed_sum)
            hidden_neuron.iput = output

        # get output for each hidden layer starting from the second layer
        for layer in self.layers[1:]:
            for hidden_neuron in layer:
                weighed_sum = Network.summation(
                    # Get inputs from previous layer
                    neurons=self.layers[self.layers.index(layer) - 1],
                    weight_set=layer.index(hidden_neuron),
                    threshold=hidden_neuron.threshold
                )
                output = hidden_neuron.activate(weighed_iput_sum=weighed_sum)
                hidden_neuron.iput = output

        for output_neuron in self.output_neurons:
            weighed_sum = Network.summation(
                neurons=self.layers[-1],
                weight_set=self.output_neurons.index(output_neuron),
                threshold=output_neuron.threshold
            )

            output = output_neuron.activate(weighed_iput_sum=weighed_sum)

        output = -1
        for output_neuron in self.output_neurons:
            if output_neuron.output > output:
                output = output_neuron.output

        return output, weighed_sum

    def back_propagate(self, error):
        """
        Back propagation algorithm for network learning.

        @params:
            self
            error - Error generated upon prediction
        """
        # Reverse layer order to do a less complicated backward iteration
        self.layers.reverse()

        # Start with output neurons
        for output_neuron in self.output_neurons:
            prev_error_grad = output_neuron.calculate_error_grad(
                error=error,
                learning_rate=self.learning_rate
            )

            layer = self.layers[0]
            for hidden_neuron in layer:
                hidden_neuron.learn(
                    error_gradient=prev_error_grad,
                    learning_rate=self.learning_rate,
                    weight_set=self.output_neurons.index(output_neuron)
                )

            # Start iterating through hidden neuron layer
            for layer in self.layers:
                # print.self.layers.index(layer)
                for hidden_neuron in layer:
                    if ((self.layers.index(layer) == 0) and (len(self.layers) > 1)):
                        weight_set = self.output_neurons.index(output_neuron)
                        prev_error_grad = hidden_neuron.calculate_error_grad(
                            prev_error_grad=prev_error_grad,
                            learning_rate=self.learning_rate
                        )

                        next_layer = self.layers[self.layers.index(layer) + 1]

                        # Adjust weight and threshold for next layer
                        for inner_neuron in next_layer:
                            inner_neuron.learn(
                                error_gradient=prev_error_grad,
                                learning_rate=self.learning_rate,
                                weight_set=layer.index(hidden_neuron)
                            )
                    else:
                        weight_set = layer.index(hidden_neuron)
                        prev_error_grad = hidden_neuron.calculate_error_grad(
                            prev_error_grad=prev_error_grad,
                            learning_rate=self.learning_rate
                        )

                        # Check if last hidden layer and set next layer
                        # as input layer else set next hidden layer
                        if self.layers.index(layer) == len(self.layers) - 1:
                            next_layer = self.iput_neurons
                        else:
                            next_layer = self.layers[self.layers.index(layer) + 1]

                        # adjust weight and threshold for next layer
                        for inner_neuron in next_layer:
                            inner_neuron.learn(
                                error_gradient=prev_error_grad,
                                learning_rate=self.learning_rate,
                                weight_set=layer.index(hidden_neuron)
                            )
        # Set layers back to normal
        self.layers.reverse()

    def geneticAlgorithm(self):

        pass

    def test(self, test_set_iput, test_set_output):
        """
        Test network after training.

        @params:
            self
            test_set_iput - Required: list of input list (list)
            test_set_output - Required: list of expected output list (list)
        """

        no_correct = 0
        weighed_sums = []
        actual_outputs = []
        sqe = 0
        for i in range(len(test_set_iput)):
            actual_output, weighed_sum = self.predict(test_set_iput[i])

            weighed_sums.append(weighed_sum)
            actual_outputs.append(actual_output)

            desired_output = test_set_output[i]
            error = desired_output - actual_output
            self.back_propagate(error=error)
            sqe = sqe + (error ** 2)

            if(self.activation_function == 'htan'):
                if ((desired_output == 0) and (actual_output <= 0)):
                    no_correct += 1
                elif((desired_output == 1) and (actual_output > 0)):
                    no_correct += 1
            else:
                if(round(actual_output) == desired_output):
                    no_correct += 1

        # print(f'No correct: {no_correct}/{len(test_set_iput)}')
        # print(f'% Correct {(no_correct/len(test_set_iput)) * 100}')

        return sqe, no_correct
        # return actual_outputs, weighed_sums

    def train(self, train_set_iput, train_set_output, max_epochs=20000, target=0.001):
        """
        Train network for classifying.

        @params:
            self
            train_set_iput   - Required: list of input list (list)
            train_set_output - Required: list of expected output list (list)
            max_epochs       - Optional: number of epochs to train for (int)
            target           - Optional: sum of squared error target to decide if network has learned (float)
        """
        sqe = 1
        epoch = 0
        bar_progress = 0
        sqes = []
        epochs = []

        while sqe > target:
            if epoch >= max_epochs:
                break
            sqe = 0
            for i in range(len(train_set_iput)):
                actual_output, b = self.predict(train_set_iput[i])
                desired_output = train_set_output[i]
                error = desired_output - actual_output
                self.back_propagate(error=error)
                sqe = round(sqe + (error * error), 5)
            sqes.append(sqe)
            epochs.append(epoch)
            epoch += 1
            bar_progress = epoch
            if sqe <= target:
                bar_progress = max_epochs
            printProgressBar(bar_progress, max_epochs, prefix='Progress:', suffix='Complete', length=25)

        return sqes, epochs

        print(f'Sum of squared errors: {sqe}')
        print(f'Epochs: {epoch}')

    def compare(self):
        """
        Print an easy to comprehend network comparison for before training and after training.

        @params:
            self
        """
        print('\n-----------------------')
        print('Network comparison')
        print('-----------------------')
        print('Keys: ')
        print(crayons.red(u'    \u25A4') + ' - Before training')
        print(crayons.green(u'    \u25A4') + ' - After training')
        print()
        print('Input layer: ')
        for i in range(len(self.iput_neurons)):
            print('    ' + crayons.red(self.old.iput_neurons[i]))
            print('    ' + crayons.green(self.iput_neurons[i]))
            print()
        print('Hidden layers: ')
        for i in range(len(self.layers)):
            layer = self.layers[i]
            print('    Layer ' + str(self.layers.index(layer)))
            for j in range(len(layer)):
                print(f'        {crayons.red(self.old.layers[i][j])}')
                print(f'        {crayons.green(self.layers[i][j])}')
                print()
        print('Outout layer: ')
        for i in range(len(self.output_neurons)):
            print(crayons.red(f'    {self.old.output_neurons[i]}'))
            print(crayons.green(f'    {self.output_neurons[i]}'))
            print()

    def details(self):
        """
        Print an easy to comprehend set of details of entire network.

        @params:
            self
        """
        print(f"No of input neurons: {self.no_iputs}")
        print(f'No of output neurons: {self.no_output_neurons}')
        print(f'No of hidden layers: {self.no_layers}')
        print(f'Learning rate: {self.learning_rate}')
        print(f'Acitvation function: {self.activation_function}\n')
        print('Input layer: ')
        for neuron in self.iput_neurons:
            print(f'    {neuron}')

        print('\nHidden layers: ')
        for layer in self.layers:
            print(f'    Layer {self.layers.index(layer) + 1}')
            for neuron in layer:
                print(f'        {neuron}')

        print('\nOutput layer')
        for neuron in self.output_neurons:
            print(f'    {neuron}')
        print()


if __name__ == '__main__':
    network = Network(3, 2, 2, 0.4, 'sigmoid')
    network.initialize()
    print(network)
